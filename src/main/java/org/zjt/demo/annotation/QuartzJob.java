package org.zjt.demo.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;


/**
 * @author <a href="juntao.zhang@bkjk.com">juntao.zhang</a>
 * @Description:
 * @Package com.bkjk.platform.uc.sync.annotation
 * @date 2018/4/3 16:20
 * @see
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface QuartzJob {

    String cron();

    int delay() default 10000;

    String id();
}

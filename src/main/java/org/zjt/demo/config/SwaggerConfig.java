package org.zjt.demo.config;

import org.zjt.demo.common.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 功能描述:Swagger与springboot集成配置类
 *
 * @author sunchangji
 * @date 2016/11/14
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${swagger.demo.swaggerEnable}")
	private Boolean swaggerEnable;

	@Value("${swagger.demo.title}")
	private String title;

	@Value("${swagger.demo.description}")
	private String description;

	@Value("${swagger.demo.contact}")
	private String contact;

	@Value("${swagger.demo.version}")
	private String version;


	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.enable(swaggerEnable)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage(Constants.AppConfigConstants.SWAGGER_BASE_PACKAGE))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(title)
				.description(description)
				.termsOfServiceUrl("")
				.contact(contact)
				.version(version)
				.build();
	}
}

package org.zjt.demo.config;

import lombok.Data;
import org.apache.http.util.Asserts;
import org.quartz.*;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * DESC
 *
 * @author
 * @create 2017-04-17 下午2:25
 **/
@Configuration
@EnableConfigurationProperties(QuartzProperties.class)
public class SchedulerConfig {


    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource, DataSourceTransactionManager dataSourceTransactionManager, QuartzProperties quartzProperties, AutowiringSpringBeanJobFactory autowiringSpringBeanJobFactory) {
        Assert.notNull(dataSource, "datasource ");
        Assert.notNull(dataSourceTransactionManager, "dataSourceTransactionManager ");
        Assert.notNull(quartzProperties, "quartzProperties ");
        Assert.notNull(quartzProperties.getJobs(), "quartzProperties Jobs");
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setDataSource(dataSource);
        ExecutorService executorService = new ThreadPoolExecutor(1, 5, 60, TimeUnit.SECONDS, new LinkedBlockingDeque<>(), new ScheduleThreadFactory());
        schedulerFactoryBean.setTaskExecutor(executorService);
        schedulerFactoryBean.setTransactionManager(dataSourceTransactionManager);
        schedulerFactoryBean.setConfigLocation(new ClassPathResource("quartz.properties"));
        schedulerFactoryBean.setAutoStartup(true);
        schedulerFactoryBean.setStartupDelay(30);
        schedulerFactoryBean.setOverwriteExistingJobs(true);
        schedulerFactoryBean.setSchedulerName("quartzScheduler");
        schedulerFactoryBean.setJobFactory(autowiringSpringBeanJobFactory);
        List<JobDetail> jobDetails = quartzProperties.getJobs().stream().map(this::jobProperties2JobDetail).collect(Collectors.toList());
        List<CronTrigger> cronTriggers = quartzProperties.getJobs().stream().map(this::jobProperties2ConTrigger).collect(Collectors.toList());
        schedulerFactoryBean.setJobDetails(jobDetails.toArray(new JobDetail[jobDetails.size()]));
        schedulerFactoryBean.setTriggers(cronTriggers.toArray(new CronTrigger[cronTriggers.size()]));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> executorService.shutdown()));
        return schedulerFactoryBean;
    }


    public CronTrigger jobProperties2ConTrigger(QuartzProperties.JobProperties jobProperties) {
        Asserts.notEmpty(jobProperties.getCron(), jobProperties + "JobProperties Cron ");
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(jobProperties.getCron());
        CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(jobProperties.getId())
                .withSchedule(scheduleBuilder.withMisfireHandlingInstructionFireAndProceed()).build();
        return trigger;
    }

    public JobDetail jobProperties2JobDetail(QuartzProperties.JobProperties jobProperties)  {
        try {
            Asserts.notNull(jobProperties, "jobProperties");
            Class<?> aClass = Class.forName(jobProperties.getClazz());
            Asserts.check(Job.class.isAssignableFrom(aClass), aClass + "未实现 org.quartz.Job 接口");
            Asserts.notEmpty(jobProperties.getId(), jobProperties + "JobProperties id ");
            JobDetail jobDetail = JobBuilder.newJob((Class<? extends Job>) aClass).withIdentity(jobProperties.getId()).build();
            if (!CollectionUtils.isEmpty(jobProperties.getJobDataMap()))
                jobDetail.getJobDataMap().putAll(jobProperties.getJobDataMap());
            return jobDetail;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Bean
    public AutowiringSpringBeanJobFactory autowiringSpringBeanJobFactory() {
        return new AutowiringSpringBeanJobFactory();
    }

}


class AutowiringSpringBeanJobFactory extends SpringBeanJobFactory implements ApplicationContextAware {
    private transient AutowireCapableBeanFactory beanFactory;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        beanFactory = applicationContext.getAutowireCapableBeanFactory();
    }

    @Override
    protected Object createJobInstance(final TriggerFiredBundle bundle) throws Exception {
        final Object job = super.createJobInstance(bundle);
        beanFactory.autowireBean(job);
        return job;
    }
}


@Data
@ConfigurationProperties(prefix = "quartz.scheduler")
class QuartzProperties {

    private Set<JobProperties> jobs;

    @Data
    class JobProperties {
        private String clazz;

        private String cron;

        private Integer delay;

        private String id;

        private Map<String, Object> JobDataMap;
    }

}


class ScheduleThreadFactory implements ThreadFactory {

    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;

    ScheduleThreadFactory() {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        namePrefix = "schedule-thread-" + poolNumber.getAndIncrement();
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }
}
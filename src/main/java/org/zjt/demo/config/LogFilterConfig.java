package org.zjt.demo.config;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
/**
 * Created by zhangjuntao on 2017/3/9.
 */

public class LogFilterConfig extends Filter<ILoggingEvent> {
    @Override
    public FilterReply decide(ILoggingEvent event) {
        if(event.getMessage().contains("hello world")){
            return FilterReply.DENY;
        }
        return FilterReply.NEUTRAL;
    }
}
package org.zjt.demo.config.properties;

import lombok.Data;

import java.util.Map;

/**
 * @author <a href="juntao.zhang@bkjk.com">juntao.zhang</a>
 * @Description:
 * @Package com.bkjk.platform.uc.web.config.properties
 * @date 2018/4/3 14:14
 * @see
 */
@Data
public class JobProperties {
    private String clazz;

    private String cron;

    private Integer delay;

    private String id;

    private Map<String, Object> jobDataMap;
}

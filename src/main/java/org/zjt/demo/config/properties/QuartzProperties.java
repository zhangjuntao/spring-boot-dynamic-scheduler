package org.zjt.demo.config.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author <a href="juntao.zhang@bkjk.com">juntao.zhang</a>
 * @Description:
 * @Package com.bkjk.platform.uc.web.config.properties
 * @date 2018/4/3 14:13
 * @see
 */
@Data
@ConfigurationProperties(prefix = "quartz.scheduler")
public class QuartzProperties {

    private List<JobProperties> jobs;


    public QuartzProperties() {
    }

    public QuartzProperties(List<JobProperties> jobs) {
        this.jobs = jobs;
    }
}
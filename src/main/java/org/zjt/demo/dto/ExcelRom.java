package org.zjt.demo.dto;

import java.io.Serializable;

/**
 * Created by zhangjuntao on 2017/3/24.
 */
public class ExcelRom implements Serializable{

    private  String userName ;

    private  String passWord ;

    private  String mailHost;

    private  String createTmie;

    private  int retry;

    private  int status;

    private  Integer valid;

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getMailHost() {
        return mailHost;
    }

    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    public String getCreateTmie() {
        return createTmie;
    }

    public void setCreateTmie(String createTmie) {
        this.createTmie = createTmie;
    }
}

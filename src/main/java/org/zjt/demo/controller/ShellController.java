package org.zjt.demo.controller;
import com.alibaba.fastjson.JSONObject;
import org.zjt.demo.common.Constants;
import org.zjt.demo.util.ExcelParser;
import org.zjt.demo.dto.ExcelRom;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

@RestController
@SuppressWarnings("all")
@RequestMapping("/shell")
@Api(value = "上传壳excel接口")
public class ShellController {
    private  static final Logger LOGGER = LoggerFactory.getLogger(ShellController.class);

    @PostMapping("/readExcel")
    public String readExcel(
            @ApiParam(value="excel文件",required = true)@RequestParam(value = "file") MultipartFile file,
            HttpServletRequest req, HttpServletResponse resp){
        try {
            if ( Constants.CAN_UPDATE_SHELL.equals(Boolean.FALSE) )
                return "";
            Constants.CAN_UPDATE_SHELL.set(Boolean.FALSE);
            Set<ExcelRom> ramXmlRomSet = Constants.RAW_EXCEL_SET ;
            List<String[]> excels = ExcelParser.readExcel(file);
            for( String[] row:excels ){
                System.out.println(JSONObject.toJSONString(row));
            }
        } catch (Exception e) {
            LOGGER.info("读取excel文件失败", e);
        }finally {
            return " ";
        }
    }



}

package org.zjt.demo.controller;

import org.zjt.demo.common.ErrorMsgEnum;
import org.zjt.demo.common.JsonResult;
import io.swagger.annotations.*;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;


/**
 * Created by Administrator on 2017/2/28.
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/api")
@Api(value = "api接口")
public class SwaggerController {

    @ApiOperation(value="根据年龄获取用户list", notes="0228 ApiParam")
    @GetMapping(value = "list")
    public JsonResult<String> test(
            @ApiParam(value="年龄",required = true)@RequestParam(value = "age",required = true)Integer age, HttpSession session){
        Assert.notNull(age,"age is empty");
        session.setAttribute("NAME","ZAHSGFSJFLKSJDKL ");
        session.getAttribute("NAME");
        return JsonResult.getSuccessResult("zhangjuntao:"+age,"ok");
    }



   /* @ApiOperation(value = "得到用户的信息（pojo）",notes = "pojo序列化")
    @GetMapping(value = "get_pojo")
    @ApiImplicitParams(
            @ApiImplicitParam(value = "用户信息",required = false,dataType = "TestPOJO",name = "testPojo")
    )
    public JsonResult<TestPOJO> getPOJO(
            @ApiParam(value="年龄",required = true) @RequestParam(value = "age",required = true)Integer age,
            @ModelAttribute TestPOJO testPojo){
        Assert.notNull(age,"age is empty");
        return JsonResult.getSuccessResult(testPojo,"ok");
    }*/



    @ApiOperation(value = "删除用户的id号码",notes = "请注意删除不能恢复")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
    public JsonResult<String> delete(@ApiParam(value="id",required = true) @PathVariable Integer id){
        Assert.notNull(id,"id is empty");
        return ErrorMsgEnum.ACTION_FAIL.getJsonResult();
    }




    @ApiOperation(value = "通过id得到用的信息",notes = "通过id得到用户信息")
    @ApiImplicitParam(value = "得到的用户id", name = "id",dataType = "Integer",paramType = "path",required = true)
    @PostMapping(value = "/get/{id}")
    public JsonResult<String> getPost( @PathVariable Integer id){
        Assert.notNull(id,"id is empty");
        return ErrorMsgEnum.NOT_FOUND.getJsonResult();
    }




}

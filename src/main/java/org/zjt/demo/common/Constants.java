package org.zjt.demo.common;


import org.zjt.demo.dto.ExcelRom;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by zhangjuntao on 2017/3/24.
 */
public class Constants {

    /**
     * 将excel存放的信息
     */
    public  static Set<ExcelRom> RAW_EXCEL_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();

    /**
     * 存放处理过email的壳
     */
    public  static Set<ExcelRom> TREATED_EXCEL_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();

    /**
     *
     */
    public  static Set<ExcelRom> TREATED_EMAIL_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();

    /**
     *
     */
    public  static Set<ExcelRom> TREATED_CFT_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();


    public  static Set<ExcelRom> TREATED_ALL_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();


    public  static Set<ExcelRom> TREATED_FAIL_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();


    public  static Set<ExcelRom> TREATED_SUCCESS_SET  = ConcurrentHashMap.<ExcelRom>newKeySet();


    /**
     * 1、整个流程执行完毕后，才会将该值为true。
     * 2、当CAN_UPDATE_SHELL为false，所有消费者线程一直阻塞。
     * 3、当CAN_UPDATE_SHELL为false，消费者一直从队列中拿出rom消费。
     * 4、当最后处理完成的，和无效的都放到TREATED_ALL_SET中。
     */
    public  static AtomicBoolean CAN_UPDATE_SHELL = new AtomicBoolean(Boolean.TRUE);

    public  static AtomicBoolean TREATED_COMPLETE = new AtomicBoolean(Boolean.FALSE);


    /**
     * config配置文件
     */
    public static class AppConfigConstants {
        public static final String DAO_BASE_PACKAGE="org.zjt.demo.dao";
        public static final String SESSION_FACTORY="sqlSessionFactory";
        public static final String MODEL_BASE_PACKAGE="org.zjt.demo.model";
        public static final String MYBAITS_XML_RESOURCES="classpath:/mybaits/mapping/*.xml";
        public static final String SWAGGER_BASE_PACKAGE="org.zjt";
        public static final String WEB_STATIC_RESOURCES_LOCATION="classpath:/static/";
    }



    /**
     * storm配置信息
     */
    public static class StormConstants{

        public static final String HANDLER_FEILD_NAME = "processed";
        public static final String SPOUT_FEILD_NAME = "raw";
        public static final String OUT_BOLT_NAME = "out_bolt";
        public static final String DATA_SPOUT_NAME = "data_spout";
        public static final String HANDLER_BOLT_NAME = "handler_bolt";
        public static final String TOPO_NAME = "topo1";
        public static final int RETRY_EMIT = 3;

    }


}




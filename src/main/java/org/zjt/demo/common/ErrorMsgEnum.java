package org.zjt.demo.common;

/**
 * Created by zhang on 2017/3/2.
 *
 * 所有返回错误信息，在该枚举中定义
 *
 */
public enum ErrorMsgEnum {

    NOT_FOUND(404,"未找到资源"),
    ACTION_FAIL(500,"操作失败"),
    UNAUTHORIZED(403,"无访问权限");

    private JsonResult<String> jsonResult;


    ErrorMsgEnum(Integer errorCode, String msg) {
        jsonResult=JsonResult.getErrorResult(msg,errorCode);
    }


    public JsonResult<String> getJsonResult() {
        return jsonResult;
    }

}


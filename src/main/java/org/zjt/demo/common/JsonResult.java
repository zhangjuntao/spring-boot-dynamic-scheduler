package org.zjt.demo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 * Created by zhang on 2017/2/28.
 */
@ApiModel("返回的json对象")
public class JsonResult<T> implements Serializable{

    public static final String SUCCESS_MSG="ok";

    private T data;

    @ApiModelProperty("请求是否成功 true:成功  false：失败")
    private Boolean success;

    @ApiModelProperty("返回的信息")
    private String msg;

    @ApiModelProperty("是否存在data数据 true：是  false：否")
    private Boolean have_data;

    @ApiModelProperty("错误码")
    private Integer error_code;

    private JsonResult(){}

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean isHave_data() {
        return have_data;
    }

    public void setHave_data(Boolean have_data) {
        this.have_data = have_data;
    }


    public static <T>JsonResult<T> getSuccessResult(T data,String msg){
        JsonResult<T> result = new JsonResult<T>();
        result.setHave_data(data !=null ? true :false);
        result.setData(data);
        result.setSuccess(true);
        result.setMsg(msg);
        result.setError_code(0);
        return result;
    }


    public static <T>JsonResult<T> getErrorResult(String msg,Integer errorCode){
        JsonResult<T> result = new JsonResult<T>();
        result.setSuccess(false);
        result.setMsg(msg);
        result.setHave_data(Boolean.FALSE);
        result.setError_code(errorCode);
        return result;
    }
}

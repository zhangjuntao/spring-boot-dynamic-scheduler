package org.zjt.demo.service;

import org.zjt.demo.dto.ExcelRom;

import java.util.List;

/**
 * Created by Administrator on 2017/3/25.
 */
public interface IHandleShellService {

    public boolean handleSpoutByStorm(List<ExcelRom> data);

}

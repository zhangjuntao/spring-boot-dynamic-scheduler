package org.zjt.demo.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * DESC
 *
 * @author
 * @create 2017-04-17 下午2:37
 **/
public class WorkTask implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String name = jobExecutionContext.getMergedJobDataMap().get("user_name").toString();
        String age = jobExecutionContext.getMergedJobDataMap().get("user_age").toString();
        System.out.println(String.format("WorkTask任务被执行！！！ 执行者：%s\t年龄：%s",name,age));
    }
}

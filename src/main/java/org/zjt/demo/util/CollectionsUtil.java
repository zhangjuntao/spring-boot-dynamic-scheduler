package org.zjt.demo.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 功能描述:集合相关的操作
 *
 * @author sunchangji
 * @date 2016/10/24
 */
public class CollectionsUtil {

    /**
     * 删除重复的
     * 求两个集合无重复的并集,不改变原有的集合
     * @param list1
     * @param list2
     * @param <T>
     * @return
     */
    public static <T> List<T> union(List<T> list1, List<T> list2) {
        List<T> newList1 = new ArrayList<T>(list1);
        List<T> newList2 = new ArrayList<T>(list2);
        // 删除在list1中出现的元素
        newList2.removeAll(newList1);
        // 把剩余的list2元素加到list1中
        newList1.addAll(newList2);
        return newList1;
    }

    /**
     * 得到不同的
     * 求两个集合的差集，不改变原来的集合
     * @param list1
     * @param list2
     * @param <T>
     * @return
     */
    public static <T> List<T> differenceSet(List<T> list1, List<T> list2) {
        List<T> newList1 = new ArrayList<T>(list1);
        List<T> newList2 = new ArrayList<T>(list2);
        newList1.removeAll(newList2);
        return newList1;
    }

    /**
     * 留下相同的
     * 求两个集合交集，不改变原有集合
     * @param list1
     * @param list2
     * @param <T>
     * @return
     */
    public static <T> List<T> intersection(List<T> list1, List<T> list2) {
        List<T> newList1 = new ArrayList<T>(list1);
        List<T> newList2 = new ArrayList<T>(list2);
        newList1.retainAll(newList2);
        return newList1;
    }




}
